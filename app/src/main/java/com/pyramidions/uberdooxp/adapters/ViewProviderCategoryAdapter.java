package com.pyramidions.uberdooxp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pyramidions.uberdooxp.R;
import com.pyramidions.uberdooxp.activities.ViewCategory;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by karthik on 12/10/17.
 */
public class ViewProviderCategoryAdapter extends RecyclerView.Adapter<ViewProviderCategoryAdapter.MyViewHolder> {
    private Context context;
    private JSONArray categoryDetails;

    public ViewProviderCategoryAdapter(Context context, JSONArray categoryDetails) {
        this.context = context;
        this.categoryDetails = categoryDetails;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView categoryName, experience, pricePerHour, quickPitch, categoryMainName;


        MyViewHolder(View view) {
            super(view);
            categoryName = (TextView) view.findViewById(R.id.categoryName);
            experience = (TextView) view.findViewById(R.id.experience);
            pricePerHour = (TextView) view.findViewById(R.id.pricePerHour);
            quickPitch = (TextView) view.findViewById(R.id.quickPitch);
            categoryMainName = (TextView) view.findViewById(R.id.categoryMainName);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_items_providers, parent, false);

        return new MyViewHolder(itemView);
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final JSONObject jsonObject = categoryDetails.optJSONObject(position);

        if (jsonObject.optString("status").equalsIgnoreCase("0")) {
            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));


        } else {
            String catname = jsonObject.optString("sub_category_name");
            String experience = jsonObject.optString("experience");
            String priceperhour = jsonObject.optString("priceperhour");
            String quickpitch = jsonObject.optString("quickpitch");
            String categoryMainName = jsonObject.optString("categoryMainName");

            holder.categoryName.setText(catname);
            holder.experience.setText(experience);
            holder.pricePerHour.setText(priceperhour);
            holder.quickPitch.setText(quickpitch);
            holder.categoryMainName.setText(categoryMainName);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewCategory.showEditDialog(context, jsonObject, position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return categoryDetails.length();
    }
}
