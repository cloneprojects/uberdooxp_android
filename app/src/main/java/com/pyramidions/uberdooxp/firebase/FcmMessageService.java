package com.pyramidions.uberdooxp.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pyramidions.uberdooxp.Events.Status;
import com.pyramidions.uberdooxp.R;
import com.pyramidions.uberdooxp.activities.MainActivity;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

/**
 * Created by user on 25-10-2017.
 */

public class FcmMessageService extends FirebaseMessagingService {

    private static final String TAG = FcmMessageService.class.getSimpleName();


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        JSONObject jsonObject = new JSONObject(remoteMessage.getData());

        sendNotification(jsonObject);
        EventBus.getDefault().post(new Status());


    }

    private void sendNotification(JSONObject jsonObject) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("type", "new");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentTitle(jsonObject.optString("title"))
                .setContentText(jsonObject.optString("message"))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


}