package com.pyramidions.uberdooxp.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.pyramidions.uberdooxp.firebase.LocationService;
import com.pyramidions.uberdooxp.R;
import com.pyramidions.uberdooxp.Volley.ApiCall;
import com.pyramidions.uberdooxp.Volley.VolleyCallback;
import com.pyramidions.uberdooxp.fragments.AccountsFragment;
import com.pyramidions.uberdooxp.fragments.HomeFragment;
import com.pyramidions.uberdooxp.helpers.AppSettings;
import com.pyramidions.uberdooxp.helpers.SharedHelper;
import com.pyramidions.uberdooxp.helpers.UrlHelper;
import com.pyramidions.uberdooxp.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener, AccountsFragment.OnFragmentInteractionListener {

    BottomSheetDialog bottomSheetDialog;
    BottomSheetBehavior bottomSheetBehavior;
    View bottomSheetView;
    public static JSONArray acceptedArray = new JSONArray();
    private String TAG = MainActivity.class.getSimpleName();
    AppSettings appSettings = new AppSettings(MainActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(MainActivity.this, "theme_value");
        Utils.SetTheme(MainActivity.this, theme_value);
        setContentView(R.layout.activity_main);
        ViewPager mainPager = (ViewPager) findViewById(R.id.mainPager);
//        CustomNavigationTabStrip tabStrip = (CustomNavigationTabStrip)findViewById(R.id.tabStrip);
        NavigationTabStrip tabStrip = (NavigationTabStrip) findViewById(R.id.tabStrip);
        Utils.setStripColor(MainActivity.this, tabStrip);
        String[] titles = new String[]{getResources().getString(R.string.home), getResources().getString(R.string.account)};
        tabStrip.setTitles(titles);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        MainPagerAdapter mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
        mainPager.setAdapter(mainPagerAdapter);
        if (getIntent().getStringExtra("type").equalsIgnoreCase("theme_changed")) {
            mainPager.setCurrentItem(1);
        } else {
            mainPager.setCurrentItem(0);
        }

        tabStrip.setViewPager(mainPager);

        postToken();
        Intent intent = new Intent(MainActivity.this, LocationService.class);
        startService(intent);

//        showRequestDialog(response);


    }


    private void postToken() {
        ApiCall.PostMethodHeaders(MainActivity.this, UrlHelper.UPDATE_DEVICE_TOKEN, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {


            }
        });
    }


    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fcm_token", appSettings.getFireBaseToken());
            jsonObject.put("os", "android");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public class MainPagerAdapter extends FragmentStatePagerAdapter {


        public MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return HomeFragment.newInstance(getResources().getString(R.string.home), "Fragment");
                case 1:
                    return AccountsFragment.newInstance(getResources().getString(R.string.account), "Fragment");
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
