package com.pyramidions.uberdooxp.activities;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pyramidions.uberdooxp.R;
import com.pyramidions.uberdooxp.Volley.ApiCall;
import com.pyramidions.uberdooxp.Volley.VolleyCallback;
import com.pyramidions.uberdooxp.adapters.ScheduleSlotAdapter;
import com.pyramidions.uberdooxp.helpers.AppSettings;
import com.pyramidions.uberdooxp.helpers.SharedHelper;
import com.pyramidions.uberdooxp.helpers.UrlHelper;
import com.pyramidions.uberdooxp.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ViewSchedule extends AppCompatActivity implements View.OnClickListener {
    public static TextView mondayView, tuesdayView, wednesdayView, thursdayView, fridayView, saturdayView, sundayView;
    public static int selectedTime;
    public static JSONArray AdapterArray = new JSONArray();
    public static JSONArray finalArray = new JSONArray();
    ScheduleSlotAdapter timeSlotAdapter;
    String mon = "", tue = "", wed = "", thu = "", fri = "", sat = "", sun = "";
    Button submitButton;
    String provider_id;
    private AppSettings appSettings;
    private JSONArray jsonArray = new JSONArray();
    private JSONArray fullTimejsonArray = new JSONArray();
    private String TAG = ViewSchedule.class.getSimpleName();
    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(ViewSchedule.this, "theme_value");
        Utils.SetTheme(ViewSchedule.this, theme_value);
        setContentView(R.layout.activity_view_schedule);

        getMyTimes();
        initViews();
        initListners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        finalArray = new JSONArray();
        getMyTimes();
    }

    private void getMyTimes() {
        ApiCall.getMethodHeaders(ViewSchedule.this, UrlHelper.VIEW_SCHEDULES, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "myVales:" + response);
                jsonArray = response.optJSONArray("schedules");
//                getTimeslotApi();
                if (jsonArray.length() != 0) {
                    provider_id = jsonArray.optJSONObject(0).optString("provider_id");
                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = new JSONObject();
                    object = jsonArray.optJSONObject(i);
                    if (object.optString("status").equalsIgnoreCase("1")) {
                        if (object.optString("days").equalsIgnoreCase("Mon")) {
                            if (mon.length() == 0) {
                                mon = object.optString("timing");
                            } else {
                                mon = mon + "," + object.optString("timing");
                            }
                        }


                        if (object.optString("days").equalsIgnoreCase("Tue")) {
                            if (tue.length() == 0) {
                                tue = object.optString("timing");
                            } else {
                                tue = tue + "," + object.optString("timing");
                            }
                        }

                        if (object.optString("days").equalsIgnoreCase("Wed")) {
                            if (wed.length() == 0) {
                                wed = object.optString("timing");
                            } else {
                                wed = wed + "," + object.optString("timing");
                            }
                        }
                        if (object.optString("days").equalsIgnoreCase("Thu")) {
                            if (thu.length() == 0) {
                                thu = object.optString("timing");
                            } else {
                                thu = thu + "," + object.optString("timing");
                            }
                        }

                        if (object.optString("days").equalsIgnoreCase("Fri")) {
                            if (fri.length() == 0) {
                                fri = object.optString("timing");
                            } else {
                                fri = fri + "," + object.optString("timing");
                            }
                        }

                        if (object.optString("days").equalsIgnoreCase("Sat")) {

                            if (sat.length() == 0) {
                                sat = object.optString("timing");
                            } else {
                                sat = sat + "," + object.optString("timing");
                            }

                        }

                        if (object.optString("days").equalsIgnoreCase("Sun")) {

                            if (sun.length() == 0) {
                                sun = object.optString("timing");
                            } else {
                                sun = sun + "," + object.optString("timing");
                            }

                        }
                    }
                }
                Log.d(TAG, "getTimes: " + mon + "," + tue + "," + wed + "," + thu + "," + fri + "," + sat + "," + sun);
                setTexts(mondayView, mon);
                setTexts(tuesdayView, tue);
                setTexts(wednesdayView, wed);
                setTexts(thursdayView, thu);
                setTexts(fridayView, fri);
                setTexts(saturdayView, sat);
                setTexts(sundayView, sun);
                getTimeslotApi();

            }
        });
    }


    private void setTexts(TextView textView, String text) {

        if (text.length() == 0) {
            setDefaultTimeslots(textView);
        } else {
            setSelectedTimeslots(textView, text);
        }


    }

    private void initViews() {
        appSettings = new AppSettings(ViewSchedule.this);
        mondayView = (TextView) findViewById(R.id.mondayView);
        tuesdayView = (TextView) findViewById(R.id.tuesdayView);
        wednesdayView = (TextView) findViewById(R.id.wednesdayView);
        thursdayView = (TextView) findViewById(R.id.thursdayView);
        fridayView = (TextView) findViewById(R.id.fridayView);
        saturdayView = (TextView) findViewById(R.id.saturdayView);
        sundayView = (TextView) findViewById(R.id.sundayView);

        Utils.setTextColour(ViewSchedule.this, mondayView);
        Utils.setTextColour(ViewSchedule.this, tuesdayView);
        Utils.setTextColour(ViewSchedule.this, wednesdayView);
        Utils.setTextColour(ViewSchedule.this, thursdayView);
        Utils.setTextColour(ViewSchedule.this, fridayView);
        Utils.setTextColour(ViewSchedule.this, saturdayView);
        Utils.setTextColour(ViewSchedule.this, sundayView);

        submitButton = (Button) findViewById(R.id.submitButton);
        Utils.setCustomButton(ViewSchedule.this, submitButton);
        backButton = (ImageView) findViewById(R.id.backButton);

    }

    private void initListners() {
        mondayView.setOnClickListener(this);
        tuesdayView.setOnClickListener(this);
        wednesdayView.setOnClickListener(this);
        thursdayView.setOnClickListener(this);
        fridayView.setOnClickListener(this);
        saturdayView.setOnClickListener(this);
        sundayView.setOnClickListener(this);
        submitButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    public void setDefaultTimeslots(TextView View) {
        View.setText(getResources().getString(R.string.add_time));
        View.setTextSize(14);
        View.setTextColor(Utils.getPrimaryColor(ViewSchedule.this));
    }

    public void setSelectedTimeslots(TextView View, String text) {
        View.setText(text);
        View.setTextSize(11);
        View.setTextColor(getResources().getColor(R.color.grey));
    }


    private String getSelectedTimeslots(JSONArray timeSlotDetails) {
        String retuntext = "";
        for (int i = 0; i < timeSlotDetails.length(); i++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject = timeSlotDetails.optJSONObject(i);
            if (jsonObject.optString("selected").equalsIgnoreCase("true")) {
                if (retuntext.length() == 0) {
                    retuntext = jsonObject.optString("timing");

                } else {
                    retuntext = retuntext + "," + jsonObject.optString("timing");

                }
            }

        }

        return retuntext;


    }


    private void getTimeslotApi() {
        ApiCall.getMethod(ViewSchedule.this, UrlHelper.APP_SETTINGS, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {


                AdapterArray = response.optJSONArray("timeslots");
                Utils.log(TAG, "timeslits:" + AdapterArray);

                for (int i = 0; i < AdapterArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject = AdapterArray.optJSONObject(i);
                    try {
                        jsonObject.put("selected", "false");
                        AdapterArray.put(i, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    for (int j = 0; j < 7; j++) {
                        JSONObject jsonObject1 = new JSONObject();
                        try {
                            jsonObject1.put("time_Slots_id", jsonObject.optString("id"));
                            if (j == 0) {
                                jsonObject1.put("days", "Mon");

                            }
                            if (j == 1) {
                                jsonObject1.put("days", "Tue");
                            }
                            if (j == 2) {
                                jsonObject1.put("days", "Wed");
                            }
                            if (j == 3) {
                                jsonObject1.put("days", "Thu");
                            }
                            if (j == 4) {
                                jsonObject1.put("days", "Fri");
                            }
                            if (j == 5) {
                                jsonObject1.put("days", "Sat");
                            }
                            if (j == 6) {
                                jsonObject1.put("days", "Sun");
                            }
                            jsonObject1.put("status", "0");
                            jsonObject1.put("selected_text", "");
                            //jsonObject1.put("provider_id", provider_id);
                            jsonObject1.put("provider_id", appSettings.getProviderId());
                            jsonObject1.put("id", "");


                            for (int k = 0; k < jsonArray.length(); k++) {
                                JSONObject jsonObject2 = new JSONObject();
                                jsonObject2 = jsonArray.optJSONObject(k);
                                if (jsonObject2.optString("time_Slots_id").equalsIgnoreCase(jsonObject1.optString("time_Slots_id"))) {
                                    if (jsonObject2.optString("days").equalsIgnoreCase(jsonObject1.optString("days"))) {
                                        jsonObject1.put("status", jsonObject2.optString("status"));
                                        jsonObject1.put("id", jsonObject2.optString("id"));

                                    }
                                }
                            }

                            finalArray.put(jsonObject1);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

//                try {
//                    mergeArrays();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }

                Log.d(TAG, "onSuccess: " + AdapterArray);


            }
        });

    }


    private void showTimeSlotDialog() {
        final Dialog dialog = new Dialog(ViewSchedule.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.time_slot_dialog);
        final RecyclerView timeSlot = (RecyclerView) dialog.findViewById(R.id.timeSlot);
        TextView okayText = (TextView) dialog.findViewById(R.id.okayText);
        okayText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String text = getSelectedTimeslots(AdapterArray);
                if (selectedTime == 0) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(mondayView);
                    } else {
                        setSelectedTimeslots(mondayView, text);


                    }
                } else if (selectedTime == 1) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(tuesdayView);
                    } else {
                        setSelectedTimeslots(tuesdayView, text);


                    }
                } else if (selectedTime == 2) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(wednesdayView);

                    } else {

                        setSelectedTimeslots(wednesdayView, text);


                    }
                } else if (selectedTime == 3) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(thursdayView);
                    } else {
                        setSelectedTimeslots(thursdayView, text);


                    }
                } else if (selectedTime == 4) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(fridayView);
                    } else {
                        setSelectedTimeslots(fridayView, text);


                    }
                } else if (selectedTime == 5) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(saturdayView);
                    } else {
                        setSelectedTimeslots(saturdayView, text);


                    }
                } else if (selectedTime == 6) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(sundayView);
                    } else {
                        setSelectedTimeslots(sundayView, text);


                    }
                }

                dialog.dismiss();
            }
        });
        try {
            for (int i = 0; i < AdapterArray.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject = AdapterArray.optJSONObject(i);
                jsonObject.put("selected", "false");

                for (int j = 0; j < finalArray.length(); j++) {
                    String check_tex;
                    if (selectedTime == 0) {
                        check_tex = "Mon";
                    } else if (selectedTime == 1) {
                        check_tex = "Tue";
                    } else if (selectedTime == 2) {
                        check_tex = "Wed";
                    } else if (selectedTime == 3) {
                        check_tex = "Thu";
                    } else if (selectedTime == 4) {
                        check_tex = "Fri";
                    } else if (selectedTime == 5) {
                        check_tex = "Sat";
                    } else {
                        check_tex = "Sun";
                    }
                    JSONObject jsonObject1 = new JSONObject();
                    jsonObject1 = finalArray.optJSONObject(j);
//                        if (j<7) {
                    if (jsonObject1.optString("time_Slots_id").equalsIgnoreCase(jsonObject.optString("id"))) {
                        if (jsonObject1.optString("days").equalsIgnoreCase(check_tex)) {
                            if (jsonObject1.optString("status").equalsIgnoreCase("0")) {
                                jsonObject.put("selected", "false");
                            } else {
                                jsonObject.put("selected", "true");

                            }
                            break;
                        }
                    }
//                        }
//                        else {
//                            if (jsonObject1.optString("time_slots_id").equalsIgnoreCase("id")) {
//                                if (jsonObject.optString("status").equalsIgnoreCase("0")) {
//                                    jsonObject.put("selected", "false");
//                                } else {
//                                    jsonObject.put("selected", "true");
//
//                                }
//                            }
//                        }

                }
                AdapterArray.put(i, jsonObject);
                Utils.log(TAG, "valuesFinal: " + AdapterArray);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();


        timeSlotAdapter = new ScheduleSlotAdapter(ViewSchedule.this, AdapterArray, selectedTime);
        GridLayoutManager linearLayoutManager = new GridLayoutManager(ViewSchedule.this, 4, GridLayoutManager.VERTICAL, false);
        timeSlot.setLayoutManager(linearLayoutManager);
        timeSlot.setAdapter(timeSlotAdapter);


    }

    @Override
    public void onClick(View view) {

        if (view == mondayView) {
            selectedTime = 0;
            showTimeSlotDialog();
        } else if (view == tuesdayView) {
            selectedTime = 1;
            showTimeSlotDialog();
        } else if (view == wednesdayView) {
            selectedTime = 2;
            showTimeSlotDialog();
        } else if (view == thursdayView) {
            selectedTime = 3;
            showTimeSlotDialog();
        } else if (view == fridayView) {
            selectedTime = 4;
            showTimeSlotDialog();
        } else if (view == saturdayView) {
            selectedTime = 5;
            showTimeSlotDialog();
        } else if (view == sundayView) {
            selectedTime = 6;
            showTimeSlotDialog();
        } else if (view == submitButton) {
            Utils.log(TAG, "values: " + finalArray);

            try {
                postUpdatedValues();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (view == backButton) {
            finish();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void postUpdatedValues() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("schedules", getFinalArray().toString());
        ApiCall.PostMethodHeaders(ViewSchedule.this, UrlHelper.UPDATE_SCHEDULES, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                moveMainActivity();
            }
        });
    }

    private void moveMainActivity() {
        finish();
    }

    private JSONArray getFinalArray() {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < finalArray.length(); i++) {
            finalArray.optJSONObject(i).remove("selected_text");
        }
        Log.d(TAG, "getFinalArray: " + finalArray);
        return finalArray;
    }

}
