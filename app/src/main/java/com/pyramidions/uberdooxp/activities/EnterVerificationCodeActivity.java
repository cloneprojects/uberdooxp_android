package com.pyramidions.uberdooxp.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.pyramidions.uberdooxp.R;
import com.pyramidions.uberdooxp.Volley.ApiCall;
import com.pyramidions.uberdooxp.Volley.VolleyCallback;
import com.pyramidions.uberdooxp.helpers.SharedHelper;
import com.pyramidions.uberdooxp.helpers.UrlHelper;
import com.pyramidions.uberdooxp.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.pyramidions.uberdooxp.helpers.Utils.filter;


public class EnterVerificationCodeActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close;
    Button verifyButton;
    String email, otp;
    EditText otpEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(EnterVerificationCodeActivity.this, "theme_value");
        Utils.SetTheme(EnterVerificationCodeActivity.this, theme_value);
        setContentView(R.layout.activity_enter_verification_code);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }

        getIntentValues();
        initViews();
        initListners();
    }

    private void getIntentValues() {
        Intent intent = getIntent();
        email = intent.getStringExtra("emailValue");
        otp = intent.getStringExtra("otp");
    }
//    InputFilter filter = new InputFilter() {
//        public CharSequence filter(CharSequence source, int start, int end,
//                                   Spanned dest, int dstart, int dend) {
//            for (int i = start; i < end; i++) {
//                if (Character.isWhitespace(source.charAt(i))) {
//                    return "";
//                }
//            }
//            return null;
//        }
//
//    };


    private void initListners() {
        close.setOnClickListener(this);
        verifyButton.setOnClickListener(this);
    }

    private void initViews() {
        close = (ImageView) findViewById(R.id.close);
        verifyButton = (Button) findViewById(R.id.verifyButton);
        Utils.setCustomButton(EnterVerificationCodeActivity.this, verifyButton);
        otpEditText = (EditText) findViewById(R.id.otpEditText);
        otpEditText.setFilters(new InputFilter[]{filter});
        //otpEditText.setText(otp);
    }

    @Override
    public void onClick(View view) {
        if (view == close) {
            onBackPressed();
        } else if (view == verifyButton) {

            if (checkOtp().equalsIgnoreCase("true")) {
                requestOtp();
            } else {
                Utils.toast(EnterVerificationCodeActivity.this, checkOtp());
            }

        }
    }

    private void moveChangePassword() {
        Intent intent = new Intent(EnterVerificationCodeActivity.this, ResetPasswordActivity.class);
        intent.putExtra("emailValue", email);
        startActivity(intent);
    }

    private String checkOtp() {
        String val;
        if (otpEditText.getText().toString().trim().length() == 0) {
            val = getResources().getString(R.string.please_enter_valid_otp);

        } else if (otpEditText.getText().toString().trim().length() < 6) {
            val = getResources().getString(R.string.please_enter_valid_otp);

        } else {
            val = "true";
        }
        return val;
    }

    private void requestOtp() {
        moveChangePassword();
//        ApiCall.PostMethod(EnterVerificationCodeActivity.this, UrlHelper.CHECK_OTP, getInputs(),
//                new VolleyCallback() {
//                    @Override
//                    public void onSuccess(JSONObject response) {
//                    }
//                });
    }

    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(" ", otpEditText.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
