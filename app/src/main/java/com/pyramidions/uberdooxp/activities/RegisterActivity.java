package com.pyramidions.uberdooxp.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pyramidions.uberdooxp.R;
import com.pyramidions.uberdooxp.Volley.ApiCall;
import com.pyramidions.uberdooxp.Volley.VolleyCallback;
import com.pyramidions.uberdooxp.adapters.RegisterCategoryAdapter;
import com.pyramidions.uberdooxp.adapters.TimeSlotAdapter;
import com.pyramidions.uberdooxp.helpers.AppSettings;
import com.pyramidions.uberdooxp.helpers.SharedHelper;
import com.pyramidions.uberdooxp.helpers.UrlHelper;
import com.pyramidions.uberdooxp.helpers.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.pyramidions.uberdooxp.helpers.Utils.filter;
import static com.pyramidions.uberdooxp.helpers.Utils.isValidEmail;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    public static TextView mondayView, tuesdayView, wednesdayView, thursdayView, fridayView, saturdayView, sundayView;
    public static int selectedTime;

    public static JSONArray finalArray = new JSONArray();
    public static JSONArray finalCategoryArray = new JSONArray();
    public static JSONArray registerDetails = new JSONArray();
    FrameLayout address_arrow, general_arrow, register_arrow, profile_arrow, category_arrow, available_arrow;
    String[] genders;
    Spinner genderSpinner;
    LinearLayout generalLayout, addressLayout, registerLayout, profileLayout, categoryLayout, availableLayout;
    LinearLayout general, address, register, profile, category, available_layout;

    EditText firstName, lastName, userName, dateOfBirth, addressOne, addressTwo, city, zipCode, state, emailAddress, phoneNumber, passWord, confirmPassword, workExperience, aboutYou;
    LinearLayout addCategory;
    public static RecyclerView categoryItems;
    JSONArray fullCategories = new JSONArray();
    JSONArray subCategories = new JSONArray();
    String uploadImagepath = "";
    public static RegisterCategoryAdapter registerCategoryAdapter;
    TimeSlotAdapter timeSlotAdapter;
    Button bottomButton;
    private final int cameraIntent = 2;

    TextView bottomText;
    ImageView add_photos;
    int currentDetails = 0;
    int clickedDetails = 0;
    Boolean isGeneralValid = false, isAddressValid = false, isRegisterValid = false, isProfileValid = false, isCategoryValid = false, isAvailableValid = false;
    AppSettings appSettings = new AppSettings(RegisterActivity.this);
    Boolean isFirstTime = true;
    Calendar myCalendar = Calendar.getInstance();
    CircleImageView profilePicture;
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };
    DatePickerDialog datePickerDialog;
    private int CLICK_ACTION_THRESHOLD = 200;
    private float startX;
    private float startY;
    public static JSONArray categoryDetails = new JSONArray();
    private JSONArray timeSlotDetails = new JSONArray();
    public static String TAG = RegisterActivity.class.getSimpleName();
    private String[] categoryName;
    private String[] subcategoryName;
    private JSONArray categoryDetailsArray = new JSONArray();
    private JSONArray jsonArray = new JSONArray();
    private File uploadFile;
    private Uri uri;

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        dateOfBirth.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(RegisterActivity.this, "theme_value");
        Utils.SetTheme(RegisterActivity.this, theme_value);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_register);
        genders = new String[]{getResources().getString(R.string.male), getResources().getString(R.string.female), getResources().getString(R.string.other)};
        initViews();
        initAdapters();
        initListners();

        getTimeslotApi();

        if (appSettings.getPageNumber().equalsIgnoreCase("")) {
            setGeneral();
        } else {
            try {
                setValues();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            getCategoryData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        isFirstTime = false;


    }


    private void getTimeslotApi() {
        ApiCall.getMethod(RegisterActivity.this, UrlHelper.APP_SETTINGS, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                jsonArray = new JSONArray();
                JSONArray status = new JSONArray();
                jsonArray = response.optJSONArray("timeslots");
                status = response.optJSONArray("status");
                Utils.log(TAG, "timeslits:" + jsonArray);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject = jsonArray.optJSONObject(i);
                    try {
                        jsonObject.put("selected", "false");
                        jsonArray.put(i, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    for (int j = 0; j < 7; j++) {
                        JSONObject jsonObject1 = new JSONObject();
                        try {
                            jsonObject1.put("time_Slots_id", jsonObject.optString("id"));
                            if (j == 0) {
                                jsonObject1.put("days", "Mon");
                            }
                            if (j == 1) {
                                jsonObject1.put("days", "Tue");
                            }
                            if (j == 2) {
                                jsonObject1.put("days", "Wed");
                            }
                            if (j == 3) {
                                jsonObject1.put("days", "Thu");
                            }
                            if (j == 4) {
                                jsonObject1.put("days", "Fri");
                            }
                            if (j == 5) {
                                jsonObject1.put("days", "Sat");
                            }
                            if (j == 6) {
                                jsonObject1.put("days", "Sun");
                            }
                            jsonObject1.put("status", "1");
                            // jsonObject1.put("selected_text", "");
                            finalArray.put(jsonObject1);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

                Log.d(TAG, "onSuccess: " + finalArray);
                appSettings.setTimeSlots(jsonArray);


            }
        });

    }

    public void setGeneralValues() throws JSONException {
        JSONObject generalObject = new JSONObject();
        appSettings.setPageNumber("1");

        generalObject.put("first_name", firstName.getText().toString());
        generalObject.put("last_name", lastName.getText().toString());
        generalObject.put("user_name", userName.getText().toString());
        generalObject.put("dob", dateOfBirth.getText().toString());
        generalObject.put("gender", genderSpinner.getSelectedItemPosition());
        registerDetails.put(0, generalObject);
    }

    public void setAddressValues() throws JSONException {
        JSONObject addressObject = new JSONObject();

        appSettings.setPageNumber("2");

        addressObject.put("address_line_one", addressOne.getText().toString());
        addressObject.put("address_line_two", addressTwo.getText().toString());
        addressObject.put("city", city.getText().toString());
        addressObject.put("state", state.getText().toString());
        addressObject.put("zipcode", zipCode.getText().toString());
        //registerDetails.put(addressObject);
        registerDetails.put(1, addressObject);
    }

    public void setRegisterValue() throws JSONException {
        JSONObject registerObject = new JSONObject();
        appSettings.setPageNumber("3");

        registerObject.put("email_address", emailAddress.getText().toString().trim());
        registerObject.put("phone", phoneNumber.getText().toString());
        registerObject.put("password", passWord.getText().toString());
        registerObject.put("confirm_password", confirmPassword.getText().toString());
        registerDetails.put(2, registerObject);
    }

    public void setProfileValue() throws JSONException {
        JSONObject profileObject = new JSONObject();
        appSettings.setPageNumber("4");


        profileObject.put("work_experience", workExperience.getText().toString());
        profileObject.put("about_you", aboutYou.getText().toString());
        registerDetails.put(3, profileObject);

    }

    public static void setCategoryValue(Context context) throws JSONException {
        JSONObject categoryObject = new JSONObject();
        AppSettings appSettings = new AppSettings(context);
        appSettings.setPageNumber("5");
        categoryObject.put("category_details", categoryDetails);
        Log.d(TAG, "setCategoryValue: " + categoryObject);
        registerDetails.put(4, categoryObject);
    }

    public void setAvailableValue() throws JSONException {
        JSONObject availableObject = new JSONObject();
        appSettings.setPageNumber("6");

        availableObject.put("monText", mondayView.getText().toString());
        availableObject.put("tueText", tuesdayView.getText().toString());
        availableObject.put("wedText", wednesdayView.getText().toString());
        availableObject.put("thuText", thursdayView.getText().toString());
        availableObject.put("friText", fridayView.getText().toString());
        availableObject.put("satText", saturdayView.getText().toString());
        availableObject.put("sunText", sundayView.getText().toString());
        registerDetails.put(5, availableObject);
    }


    private void setValues() throws Exception {
        try {

            registerDetails = appSettings.getRegisterArray();
            JSONObject generalObject = registerDetails.optJSONObject(0);
            JSONObject addressObject = registerDetails.optJSONObject(1);
            JSONObject registerObject = registerDetails.optJSONObject(2);
            JSONObject profileObject = registerDetails.optJSONObject(3);
            JSONObject categoryObject = registerDetails.optJSONObject(4);
            JSONObject availableObject = registerDetails.optJSONObject(5);


            firstName.setText(generalObject.optString("first_name"));
            lastName.setText(generalObject.optString("last_name"));
            userName.setText(generalObject.optString("user_name"));
            dateOfBirth.setText(generalObject.optString("dob"));
            genderSpinner.setSelection(Integer.parseInt(generalObject.optString("gender")));

            addressOne.setText(addressObject.optString("address_line_one"));
            addressTwo.setText(addressObject.optString("address_line_two"));
            city.setText(addressObject.optString("city"));
            state.setText(addressObject.optString("state"));
            zipCode.setText(addressObject.optString("zipcode"));

            emailAddress.setText(registerObject.optString("email_address"));
            phoneNumber.setText(registerObject.optString("phone"));
            passWord.setText(registerObject.optString("password"));
            confirmPassword.setText(registerObject.optString("confirm_password"));

            workExperience.setText(profileObject.optString("work_experience"));
            aboutYou.setText(profileObject.optString("about_you"));

          /*  if (categoryObject.length() > 0) {
                categoryDetails = categoryObject.optJSONArray("category_details");
            } else {

            }*/

            categoryDetails = new JSONArray();
            if (categoryDetails.length() > 0) {
                registerCategoryAdapter = new RegisterCategoryAdapter(RegisterActivity.this, categoryDetails);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RegisterActivity.this, LinearLayoutManager.HORIZONTAL, false);
                categoryItems.setLayoutManager(linearLayoutManager);
                categoryItems.setAdapter(registerCategoryAdapter);
            }
        } catch (Exception e) {
            categoryDetails = new JSONArray();

        }

        if (appSettings.getPageNumber().equalsIgnoreCase("1")) {
            setGeneral();
            validGeneral();
            setAlpha();
        } else if (appSettings.getPageNumber().equalsIgnoreCase("2")) {
            isGeneralValid = true;
            setAddress();
            validAddress();
            setAlpha();
        } else if (appSettings.getPageNumber().equalsIgnoreCase("3")) {
            isGeneralValid = true;
            isAddressValid = true;
            setRegister();
            validRegister();
            setAlpha();

        } else if (appSettings.getPageNumber().equalsIgnoreCase("4")) {
            isGeneralValid = true;
            isAddressValid = true;
            isRegisterValid = true;
            setProfile();
            validProfile();
            setAlpha();
        } else if (appSettings.getPageNumber().equalsIgnoreCase("5")) {
            isGeneralValid = true;
            isAddressValid = true;
            isRegisterValid = true;
            isProfileValid = true;
            setCategory();
            validCategory();
            setAlpha();

        } else if (appSettings.getPageNumber().equalsIgnoreCase("6")) {
            isGeneralValid = true;
            isAddressValid = true;
            isRegisterValid = true;
            isProfileValid = true;
            isCategoryValid = true;
            setAvailable();
            setAlpha();
        }


    }

//    private void getTimeSlot() throws JSONException {
//        timeSlotDetails = new JSONArray();
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("time_slot", "9.00-10.00am");
//        jsonObject.put("isSelected", "1");
//        timeSlotDetails.put(jsonObject);
//
//        jsonObject = new JSONObject();
//        jsonObject.put("time_slot", "11.00-12.00pm");
//        jsonObject.put("isSelected", "0");
//        timeSlotDetails.put(jsonObject);
//
//        jsonObject = new JSONObject();
//        jsonObject.put("time_slot", "1.00-2.00pm");
//        jsonObject.put("isSelected", "0");
//        timeSlotDetails.put(jsonObject);
//
//        jsonObject = new JSONObject();
//        jsonObject.put("time_slot", "3.00-5.00pm");
//        jsonObject.put("isSelected", "1");
//        timeSlotDetails.put(jsonObject);
//
//
//    }

    private void getCategoryData() throws JSONException {
        ApiCall.getMethod(RegisterActivity.this, UrlHelper.LIST_CATEGORY, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray list_category = new JSONArray();
                fullCategories = response.optJSONArray("list_category");


                categoryName = new String[fullCategories.length()];
                for (int i = 0; i < fullCategories.length(); i++) {
                    categoryName[i] = fullCategories.optJSONObject(i).optString("category_name");


//                    String val = jsonArray.optString(i);
//                    JSONObject jsonObject1 = new JSONObject();
//                    try {
//                        jsonObject1.put("name", val);
//
//                        JSONArray jsonArray1 = new JSONArray();
//                        jsonArray1 = jsonObject.optJSONArray(val);
//                        jsonObject1.put("subCategories", jsonArray1);
//                        categoryDetailsArray.put(jsonObject1);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }


                }
            }
        });

        Utils.log(TAG, "values:" + categoryDetailsArray);

        registerCategoryAdapter.notifyDataSetChanged();


    }

    public void getTimeSlots() {

    }

    private void initAdapters() {
        ArrayAdapter aa = new ArrayAdapter(this, R.layout.gender_items, genders);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(aa);

        registerCategoryAdapter = new RegisterCategoryAdapter(RegisterActivity.this, categoryDetails);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RegisterActivity.this, LinearLayoutManager.HORIZONTAL, false);
        categoryItems.setLayoutManager(linearLayoutManager);
        categoryItems.setAdapter(registerCategoryAdapter);


    }

    public void slideFromLeft(View view) {
        view.setVisibility(View.VISIBLE);
        view.startAnimation(AnimationUtils.loadAnimation(RegisterActivity.this, R.anim.slidefromleft));
    }

    private void initListners() {


        general.setOnClickListener(this);
        address.setOnClickListener(this);
        register.setOnClickListener(this);
        profile.setOnClickListener(this);
        category.setOnClickListener(this);
        //available_layout.setOnClickListener(this);
        addCategory.setOnClickListener(this);
        mondayView.setOnClickListener(this);
        tuesdayView.setOnClickListener(this);
        wednesdayView.setOnClickListener(this);
        thursdayView.setOnClickListener(this);
        fridayView.setOnClickListener(this);
        saturdayView.setOnClickListener(this);
        sundayView.setOnClickListener(this);
        bottomButton.setOnClickListener(this);
        profilePicture.setOnClickListener(this);
        bottomText.setOnClickListener(this);

        passWord.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());
        confirmPassword.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());
    }

    public void setAlpha() {
        if (!isAddressValid) {
            address.setAlpha((float) 0.5);
        } else {

            address.setAlpha(1);
        }
        if (!isRegisterValid) {
            register.setAlpha((float) 0.5);
        } else {

            register.setAlpha(1);
        }
        if (!isProfileValid) {
            profile.setAlpha((float) 0.5);
        } else {

            profile.setAlpha(1);
        }
        if (!isCategoryValid) {
            category.setAlpha((float) 0.5);
        } else {

            category.setAlpha(1);
        }

       /* if (!isAvailableValid) {
            //  available_layout.setAlpha((float) 0.5);
        } else {

            //available_layout.setAlpha(1);
        }*/
        if (!isGeneralValid) {
            general.setAlpha((float) 0.5);
        } else {

            general.setAlpha(1);
        }

    }

    private void setGeneral() {
        bottomButton.setText(getResources().getString(R.string.next));
        currentDetails = 0;
        bottomText.setVisibility(View.VISIBLE);
        addressLayout.setVisibility(View.GONE);
        registerLayout.setVisibility(View.GONE);
        profileLayout.setVisibility(View.GONE);
        categoryLayout.setVisibility(View.GONE);
        availableLayout.setVisibility(View.GONE);

        setAlpha();
        slideFromLeft(generalLayout);

        address_arrow.setVisibility(View.INVISIBLE);
        general_arrow.setVisibility(View.VISIBLE);
        register_arrow.setVisibility(View.INVISIBLE);
        profile_arrow.setVisibility(View.INVISIBLE);
        category_arrow.setVisibility(View.INVISIBLE);
        available_arrow.setVisibility(View.GONE);
    }

    private void setAddress() {
        currentDetails = 1;
        bottomButton.setText(getResources().getString(R.string.next));

        bottomText.setVisibility(View.VISIBLE);

        generalLayout.setVisibility(View.GONE);
        registerLayout.setVisibility(View.GONE);
        profileLayout.setVisibility(View.GONE);
        categoryLayout.setVisibility(View.GONE);
        availableLayout.setVisibility(View.GONE);

        setAlpha();

        slideFromLeft(addressLayout);
        address_arrow.setVisibility(View.VISIBLE);
        general_arrow.setVisibility(View.INVISIBLE);
        register_arrow.setVisibility(View.INVISIBLE);
        profile_arrow.setVisibility(View.INVISIBLE);
        category_arrow.setVisibility(View.INVISIBLE);
        available_arrow.setVisibility(View.GONE);
    }

    private void setRegister() {
        currentDetails = 2;
        bottomButton.setText(getResources().getString(R.string.next));

        bottomText.setVisibility(View.VISIBLE);

        generalLayout.setVisibility(View.GONE);
        addressLayout.setVisibility(View.GONE);
        profileLayout.setVisibility(View.GONE);
        categoryLayout.setVisibility(View.GONE);
        availableLayout.setVisibility(View.GONE);

        setAlpha();


        slideFromLeft(registerLayout);

        address_arrow.setVisibility(View.INVISIBLE);
        general_arrow.setVisibility(View.INVISIBLE);
        register_arrow.setVisibility(View.VISIBLE);
        profile_arrow.setVisibility(View.INVISIBLE);
        category_arrow.setVisibility(View.INVISIBLE);
        available_arrow.setVisibility(View.GONE);
    }

    private void setProfile() {
        currentDetails = 3;
        bottomButton.setText(getResources().getString(R.string.next));

        bottomText.setVisibility(View.VISIBLE);

        setAlpha();

        generalLayout.setVisibility(View.GONE);
        addressLayout.setVisibility(View.GONE);
        registerLayout.setVisibility(View.GONE);
        categoryLayout.setVisibility(View.GONE);
        availableLayout.setVisibility(View.GONE);

        slideFromLeft(profileLayout);
        address_arrow.setVisibility(View.INVISIBLE);
        general_arrow.setVisibility(View.INVISIBLE);
        register_arrow.setVisibility(View.INVISIBLE);
        profile_arrow.setVisibility(View.VISIBLE);
        category_arrow.setVisibility(View.INVISIBLE);
        available_arrow.setVisibility(View.GONE);
    }

    private void setCategory() {
        currentDetails = 4;
        bottomButton.setText(getResources().getString(R.string.finish));

        bottomText.setVisibility(View.VISIBLE);

        setAlpha();

        generalLayout.setVisibility(View.GONE);
        addressLayout.setVisibility(View.GONE);
        registerLayout.setVisibility(View.GONE);
        availableLayout.setVisibility(View.GONE);
        profileLayout.setVisibility(View.GONE);
        slideFromLeft(categoryLayout);
        address_arrow.setVisibility(View.INVISIBLE);
        general_arrow.setVisibility(View.INVISIBLE);
        register_arrow.setVisibility(View.INVISIBLE);
        profile_arrow.setVisibility(View.INVISIBLE);
        category_arrow.setVisibility(View.VISIBLE);
        available_arrow.setVisibility(View.GONE);
    }

    private void setAvailable() {
        currentDetails = 5;

        // bottomButton.setText(getResources().getString(R.string.finish));
        bottomText.setVisibility(View.VISIBLE);
        setAlpha();
        generalLayout.setVisibility(View.GONE);
        addressLayout.setVisibility(View.GONE);
        registerLayout.setVisibility(View.GONE);
        categoryLayout.setVisibility(View.VISIBLE);
        profileLayout.setVisibility(View.GONE);

        // slideFromLeft(availableLayout);


        address_arrow.setVisibility(View.INVISIBLE);
        general_arrow.setVisibility(View.INVISIBLE);
        register_arrow.setVisibility(View.INVISIBLE);
        profile_arrow.setVisibility(View.INVISIBLE);
        category_arrow.setVisibility(View.VISIBLE);
        available_arrow.setVisibility(View.GONE);
    }

    private void initViews() {
        address_arrow = (FrameLayout) findViewById(R.id.address_arrow);
        general_arrow = (FrameLayout) findViewById(R.id.general_arrow);
        register_arrow = (FrameLayout) findViewById(R.id.register_arrow);
        profile_arrow = (FrameLayout) findViewById(R.id.profile_arrow);
        category_arrow = (FrameLayout) findViewById(R.id.category_arrow);
        available_arrow = (FrameLayout) findViewById(R.id.available_arrow);

        generalLayout = (LinearLayout) findViewById(R.id.generalLayout);
        addressLayout = (LinearLayout) findViewById(R.id.addressLayout);
        registerLayout = (LinearLayout) findViewById(R.id.registerLayout);
        profileLayout = (LinearLayout) findViewById(R.id.profileLayout);
        categoryLayout = (LinearLayout) findViewById(R.id.categoryLayout);
        availableLayout = (LinearLayout) findViewById(R.id.availableLayout);

        general = (LinearLayout) findViewById(R.id.general);
        address = (LinearLayout) findViewById(R.id.address);
        register = (LinearLayout) findViewById(R.id.register);
        profile = (LinearLayout) findViewById(R.id.profile);
        category = (LinearLayout) findViewById(R.id.category);
        available_layout = (LinearLayout) findViewById(R.id.available);

        addCategory = (LinearLayout) findViewById(R.id.addCategory);
        categoryItems = (RecyclerView) findViewById(R.id.categoryItems);
        mondayView = (TextView) findViewById(R.id.mondayView);
        tuesdayView = (TextView) findViewById(R.id.tuesdayView);
        wednesdayView = (TextView) findViewById(R.id.wednesdayView);
        thursdayView = (TextView) findViewById(R.id.thursdayView);
        fridayView = (TextView) findViewById(R.id.fridayView);
        saturdayView = (TextView) findViewById(R.id.saturdayView);
        sundayView = (TextView) findViewById(R.id.sundayView);

        Utils.setTextColour(RegisterActivity.this, mondayView);
        Utils.setTextColour(RegisterActivity.this, tuesdayView);
        Utils.setTextColour(RegisterActivity.this, wednesdayView);
        Utils.setTextColour(RegisterActivity.this, thursdayView);
        Utils.setTextColour(RegisterActivity.this, fridayView);
        Utils.setTextColour(RegisterActivity.this, saturdayView);
        Utils.setTextColour(RegisterActivity.this, sundayView);
        bottomButton = (Button) findViewById(R.id.bottomButton);
        Utils.setCustomButton(RegisterActivity.this, bottomButton);

        bottomText = (TextView) findViewById(R.id.bottomText);
        add_photos = (ImageView) findViewById(R.id.add_photos);

        bottomText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToSignIn();
            }
        });
        profilePicture = (CircleImageView) findViewById(R.id.profilePicture);
        CircleImageView category_add = (CircleImageView) findViewById(R.id.category_add);
        Utils.setCircleImageView(RegisterActivity.this, profilePicture);
        Utils.setCircleImageView(RegisterActivity.this, category_add);

        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        userName = (EditText) findViewById(R.id.userName);
        dateOfBirth = (EditText) findViewById(R.id.dateOfBirth);
        addressOne = (EditText) findViewById(R.id.addressOne);
        addressTwo = (EditText) findViewById(R.id.addressTwo);
        city = (EditText) findViewById(R.id.city);
        state = (EditText) findViewById(R.id.state);
        zipCode = (EditText) findViewById(R.id.zipCode);
        emailAddress = (EditText) findViewById(R.id.emailAddress);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);
        passWord = (EditText) findViewById(R.id.passWord);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        workExperience = (EditText) findViewById(R.id.workExperience);
        aboutYou = (EditText) findViewById(R.id.aboutYou);
        genderSpinner = (Spinner) findViewById(R.id.genderSpinner);

//        InputFilter filter = new InputFilter() {
//            public CharSequence filter(CharSequence source, int start, int end,
//                                       Spanned dest, int dstart, int dend) {
//                for (int i = start; i < end; i++) {
//                    if (Character.isWhitespace(source.charAt(i))) {
//                        return "";
//                    }
//                }
//                return null;
//            }
//
//        };
        passWord.setFilters(new InputFilter[]{filter});
        confirmPassword.setFilters(new InputFilter[]{filter});

        passWord.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());
        confirmPassword.setTransformationMethod(new Utils.AsteriskPasswordTransformationMethod());
        firstName.addTextChangedListener(new GenericTextWatcher("1"));
        lastName.addTextChangedListener(new GenericTextWatcher("1"));
        userName.addTextChangedListener(new GenericTextWatcher("1"));
        dateOfBirth.addTextChangedListener(new GenericTextWatcher("1"));
        addressOne.addTextChangedListener(new GenericTextWatcher("2"));
        addressTwo.addTextChangedListener(new GenericTextWatcher("2"));
        city.addTextChangedListener(new GenericTextWatcher("2"));
        state.addTextChangedListener(new GenericTextWatcher("2"));
        zipCode.addTextChangedListener(new GenericTextWatcher("2"));
        emailAddress.addTextChangedListener(new GenericTextWatcher("3"));
        phoneNumber.addTextChangedListener(new GenericTextWatcher("3"));
        passWord.addTextChangedListener(new GenericTextWatcher("3"));
        confirmPassword.addTextChangedListener(new GenericTextWatcher("3"));
        workExperience.addTextChangedListener(new GenericTextWatcher("4"));
        aboutYou.addTextChangedListener(new GenericTextWatcher("4"));

        datePickerDialog = new DatePickerDialog(RegisterActivity.this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.updateDate(2000, 1, 0);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());


        dateOfBirth.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        startX = event.getX();
                        startY = event.getY();
                        break;
                    case MotionEvent.ACTION_UP:
                        float endX = event.getX();
                        float endY = event.getY();
                        if (isAClick(startX, endX, startY, endY)) {
                            hideKeyboard();
                            datePickerDialog.show();

                        }
                        break;
                }
                return false;
            }
        });

    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }


    private boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return !(differenceX > CLICK_ACTION_THRESHOLD/* =5 */ || differenceY > CLICK_ACTION_THRESHOLD);
    }

    public String validGeneral() {
        String value = "true";
        if (firstName.getText().length() == 0) {
            return getResources().getString(R.string.enter_firstname);
        } else if (lastName.getText().length() == 0) {
            return getResources().getString(R.string.enter_lastname);
        } else if (dateOfBirth.getText().length() == 0) {
            return getResources().getString(R.string.enter_dob);

        } else {
            isGeneralValid = true;
            return value;
        }
    }

    public String validAddress() {
        String value = "true";

        if (addressOne.getText().length() == 0) {
            return getResources().getString(R.string.enter_address);
        } else if (addressTwo.getText().length() == 0) {
            return getResources().getString(R.string.enter_address);
        } else if (city.getText().length() == 0) {
            return getResources().getString(R.string.enter_city);

        } else if (state.getText().length() == 0) {
            return getResources().getString(R.string.enter_state);

        } else if (zipCode.getText().length() == 0) {
            return getResources().getString(R.string.enter_zipcode);

        } else {
            isAddressValid = true;
            return value;
        }
    }

//    public class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod {
//        @Override
//        public CharSequence getTransformation(CharSequence source, View view) {
//            return new AsteriskPasswordTransformationMethod.PasswordCharSequence(source);
//        }
//
//        private class PasswordCharSequence implements CharSequence {
//            private CharSequence mSource;
//
//            public PasswordCharSequence(CharSequence source) {
//                mSource = source; // Store char sequence
//            }
//
//            public char charAt(int index) {
//                return '*'; // This is the important part
//            }
//
//            public int length() {
//                return mSource.length(); // Return default
//            }
//
//            public CharSequence subSequence(int start, int end) {
//                return mSource.subSequence(start, end); // Return default
//            }
//        }
//    }

//    public static boolean isValidEmail(CharSequence target) {
//        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
//    }

    public String validRegister() {
        String value = "true";

        if (emailAddress.getText().toString().trim().length() == 0 ||
                !isValidEmail(emailAddress.getText().toString().trim())) {
            return getResources().getString(R.string.please_enter_valid_email);
        } else if (phoneNumber.getText().length() == 0) {
            return getResources().getString(R.string.enter_phonenumber);

        } else if (passWord.getText().length() == 0) {
            return getResources().getString(R.string.enter_password);

        } else if (confirmPassword.getText().length() == 0) {
            return getResources().getString(R.string.enter_confirmpassword);

        } else if (!passWord.getText().toString().equalsIgnoreCase(confirmPassword.getText().toString())) {
            return getResources().getString(R.string.password_not_match);

        } else if (passWord.getText().toString().trim().length() < 6) {
            return getResources().getString(R.string.password_must_be_six);

        } else if (confirmPassword.getText().toString().trim().length() < 6) {
            return getResources().getString(R.string.password_must_be_six);
        } else {
            isRegisterValid = true;

            return value;
        }
    }


    public String validProfile() {
        String value = "true";

        if (workExperience.getText().length() == 0) {
            return getResources().getString(R.string.enter_work_experience);

        } else if (aboutYou.getText().length() == 0) {
            return getResources().getString(R.string.enter_about_you);

        } else if (uploadFile == null) {
            return getResources().getString(R.string.please_upload_an_image);
        } else {
            isProfileValid = true;
            return value;
        }
    }

    public String validCategory() {
        String value = "true";

        if (categoryDetails.length() == 0) {
            return getResources().getString(R.string.please_select_categroy);
        } else {
            isCategoryValid = true;
            return value;

        }

    }


    private String totalValidation() {
        String value = "true";

        if (currentDetails <= clickedDetails) {

            if (currentDetails == 0) {
                if (validGeneral().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validGeneral();
                }
            } else if (currentDetails == 1) {
                if (validAddress().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validAddress();
                }

            } else if (currentDetails == 2) {
                if (validRegister().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validRegister();

                }

            } else if (currentDetails == 3) {
                if (validProfile().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validProfile();
                }
            } else if (currentDetails == 4) {
                if (validCategory().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validCategory();
                }
            } else return value;
        } else {
            return value;
        }
    }





  /*  public String totalValidation() {
        String value = "true";
       isGeneralValid
        return value;

    }*/

    @Override
    public void onClick(View view) {
        if (view == general) {
            clickedDetails = 0;
            if (generalValidation().equalsIgnoreCase("true")) {
                setGeneral();
            } else {
                Toast.makeText(this, generalValidation(), Toast.LENGTH_SHORT).show();
            }

        } else if (view == address) {
            clickedDetails = 1;

            if (generalValidation().equalsIgnoreCase("true")) {
                setAddress();

            } else {
                Toast.makeText(this, generalValidation(), Toast.LENGTH_SHORT).show();
            }


        } else if (view == bottomText) {
            moveToSignIn();
        } else if (view == profilePicture) {
            showPictureDialog();
        } else if (view == register) {
            clickedDetails = 2;

            if (generalValidation().equalsIgnoreCase("true")) {
                setRegister();


            } else {
                Toast.makeText(this, generalValidation(), Toast.LENGTH_SHORT).show();
            }

        } else if (view == profile) {
            clickedDetails = 3;

            if (generalValidation().equalsIgnoreCase("true")) {
                setProfile();


            } else {
                Toast.makeText(this, generalValidation(), Toast.LENGTH_SHORT).show();
            }

        } else if (view == category) {
            clickedDetails = 4;

            if (generalValidation().equalsIgnoreCase("true")) {
                setCategory();

            } else {
                Toast.makeText(this, generalValidation(), Toast.LENGTH_SHORT).show();
            }

        } /* else if (view == available_layout) {
           clickedDetails = 5;

            if (generalValidation().equalsIgnoreCase("true")) {
                setAvailable();

            } else {
                Toast.makeText(this, generalValidation(), Toast.LENGTH_SHORT).show();
            }

        }*/ else if (view == addCategory) {
            showAddDialog();
        } else if (view == mondayView) {
            selectedTime = 0;
            showTimeSlotDialog();
        } else if (view == tuesdayView) {
            selectedTime = 1;
            showTimeSlotDialog();
        } else if (view == wednesdayView) {
            selectedTime = 2;
            showTimeSlotDialog();
        } else if (view == thursdayView) {
            selectedTime = 3;
            showTimeSlotDialog();
        } else if (view == fridayView) {
            selectedTime = 4;
            showTimeSlotDialog();
        } else if (view == saturdayView) {
            selectedTime = 5;
            showTimeSlotDialog();
        } else if (view == sundayView) {
            selectedTime = 6;
            showTimeSlotDialog();
        } else if (view == bottomButton) {
            if (currentDetails == 0) {
                if (validGeneral().equalsIgnoreCase("true")) {
                    setAddress();
                } else {
                    Toast.makeText(this, validGeneral(), Toast.LENGTH_SHORT).show();
                }
            } else if (currentDetails == 1) {
                if (validAddress().equalsIgnoreCase("true")) {
                    setRegister();
                } else {
                    Toast.makeText(this, validAddress(), Toast.LENGTH_SHORT).show();
                }

            } else if (currentDetails == 2) {
                if (validRegister().equalsIgnoreCase("true")) {
                    setProfile();
                } else {
                    Toast.makeText(this, validRegister(), Toast.LENGTH_SHORT).show();
                }

            } else if (currentDetails == 3) {
                if (validProfile().equalsIgnoreCase("true")) {
                    setCategory();
                } else {
                    Toast.makeText(this, validProfile(), Toast.LENGTH_SHORT).show();
                }
            } else if (currentDetails == 4) {
                Utils.show(RegisterActivity.this);
                if (validCategory().equalsIgnoreCase("true")) {
                    setAvailable();


                    if (validProfile().equalsIgnoreCase("true")) {
                        uploadImage();

                    } else {
                        Utils.dismiss();
                        Toast.makeText(this, validProfile(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Utils.dismiss();
                    Toast.makeText(this, validCategory(), Toast.LENGTH_SHORT).show();

                }
            }
        }
    }

    private void uploadImage() {
        // Utils.show(RegisterActivity.this);
        ApiCall.uploadImage(uploadFile, RegisterActivity.this, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    uploadImagepath = response.optString("image");
                    registerValues();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static boolean validScheudle() {
        boolean isAllSelected = false;
        for (int i = 0; i < finalArray.length(); i++) {
            if (finalArray.optJSONObject(i).optString("status").equalsIgnoreCase("1")) {
                return true;
            }
        }
        return false;
    }

    private void registerValues() throws JSONException {


        if (validScheudle()) {
            registerDetails = appSettings.getRegisterArray();

            JSONObject jsonObject = new JSONObject();

            JSONObject generalObject = registerDetails.optJSONObject(0);
            JSONObject addressObject = registerDetails.optJSONObject(1);
            JSONObject registerObject = registerDetails.optJSONObject(2);
            JSONObject profileObject = registerDetails.optJSONObject(3);
            JSONObject categoryObject = registerDetails.optJSONObject(4);
            JSONObject availableObject = registerDetails.optJSONObject(5);

            categoryDetails = categoryObject.optJSONArray("category_details");
            jsonObject.put("first_name", generalObject.optString("first_name"));
            jsonObject.put("last_name", generalObject.optString("last_name"));
            jsonObject.put("dob", generalObject.optString("dob"));

            if (Integer.parseInt(generalObject.optString("gender")) == 0) {
                jsonObject.put("gender", "M");
            } else {
                jsonObject.put("gender", "F");
            }

            jsonObject.put("address1", addressObject.optString("address_line_one"));
            jsonObject.put("address2", addressObject.optString("address_line_two"));
            jsonObject.put("city", addressObject.optString("city"));
            jsonObject.put("state", addressObject.optString("state"));
            jsonObject.put("zipcode", addressObject.optString("zipcode"));
            jsonObject.put("about", profileObject.optString("about_you"));
            jsonObject.put("workexperience", profileObject.optString("work_experience"));
            jsonObject.put("email", registerObject.optString("email_address"));
            jsonObject.put("password", registerObject.optString("password"));
            jsonObject.put("mobile", registerObject.optString("phone"));
            jsonObject.put("category", getCategoyArray().toString());
            jsonObject.put("schedules", getFinalArray().toString());
            jsonObject.put("image", uploadImagepath);

            Utils.log(TAG, "registerValues:" + registerDetails);

            ApiCall.PostMethodWithoutProgress(RegisterActivity.this, UrlHelper.SIGN_UP, jsonObject, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {


                    Log.e("asList", "before: " + appSettings.getRegisterArray().length());

                    JSONArray jsonArray = new JSONArray();
                    appSettings.setRegisterArray(jsonArray);

                    Log.e("asList", "after: " + appSettings.getRegisterArray().length());

                    isGeneralValid = false;
                    isAddressValid = false;
                    isRegisterValid = false;
                    isProfileValid = false;
                    isCategoryValid = false;

                    runOnUiThread(new Runnable() {
                        public void run() {
                            Utils.toast(RegisterActivity.this, getResources().getString(R.string.account_created_successfully));
                        }
                    });

                    moveToSignIn();
                }
            });
        } else {
            runOnUiThread(new Runnable() {
                public void run() {
                    Utils.dismiss();
                    Utils.toast(RegisterActivity.this, getResources().getString(R.string.please_select_atleast_one_time));
                }
            });
        }

    }

    private JSONArray getFinalArray() {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < finalArray.length(); i++) {
            finalArray.optJSONObject(i).remove("selected_text");
        }
        return finalArray;
    }

    private void moveToSignIn() {
        Intent signin = new Intent(RegisterActivity.this, SignInActivity.class);
        startActivity(signin);
        finish();

    }


    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                        } else {
                            takePhotoFromCamera();
                        }


                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);

    }

    private void takePhotoFromCamera() {
        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(),
                getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder,
                getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }

        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("PROFILE_IMG_" + randomImageNo);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

//        uri = Uri.fromFile(file);

//        uri = Uri.fromFile(file);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(RegisterActivity.this, getPackageName() + ".provider", file);
            Log.d(TAG, "onClick: " + uri.getPath());
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        } else {
            uri = Uri.fromFile(file);
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        }
        Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());

        /*Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, 2);*/
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(RegisterActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;

            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(RegisterActivity.this, getResources().getString(R.string.storage_permission_error));

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1://gallery
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {
                        handleimage(uri);
                    } else {
                        Utils.toast(RegisterActivity.this, getResources().getString(R.string.unable_to_select));
                    }
                }
                break;
            case cameraIntent://camera
                /*if (data != null) {
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    profilePic.setColorFilter(getResources().getColor(R.color.transparent));
                    profilePic.setImageBitmap(imageBitmap);

                    Uri tempUri = Utils.getImageUri(getApplicationContext(), imageBitmap);


                    uploadFile = new File(Utils.getRealPathFromURI(SignUpActivity.this, tempUri));
                }*/
                uploadFile = new File(String.valueOf(appSettings.getImageUploadPath()));
                if (uploadFile.exists()) {
                    add_photos.setVisibility(View.GONE);

                    Log.d(TAG, "onActivityResult: " + uploadFile.exists());
                    Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        profilePicture.setColorFilter(ContextCompat.getColor(RegisterActivity.this, R.color.transparent));
                        Glide.with(RegisterActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .dontAnimate()
                                .into(profilePicture);
                    } else {
                        profilePicture.setColorFilter(getResources().getColor(R.color.transparent));
                        Glide.with(RegisterActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .dontAnimate()
                                .into(profilePicture);
                    }
                }
                break;


        }
    }

    private void handleimage(Uri uri) {
        add_photos.setVisibility(View.GONE);

        profilePicture.setColorFilter(getResources().getColor(R.color.transparent));
        Glide.with(RegisterActivity.this)
                .load(Utils.getRealPathFromUriNew(RegisterActivity.this, uri))
                .into(profilePicture);
        uploadFile = new File(Utils.getRealPathFromURI(RegisterActivity.this, uri));

    }


    private JSONArray getCategoyArray() {


        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < categoryDetails.length(); i++) {
            JSONObject jsonObject = new JSONObject();
            categoryDetails.optJSONObject(i).remove("categoryMainName");
            categoryDetails.optJSONObject(i).remove("sub_category_name");
            jsonObject = categoryDetails.optJSONObject(i);
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

    private String generalValidation() {
        String value = "true";

        if (currentDetails <= clickedDetails) {

            if (currentDetails == 0) {
                if (validGeneral().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validGeneral();
                }
            } else if (currentDetails == 1) {
                if (validAddress().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validAddress();
                }

            } else if (currentDetails == 2) {
                if (validRegister().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validRegister();

                }

            } else if (currentDetails == 3) {
                if (validProfile().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validProfile();
                }
            } else if (currentDetails == 4) {
                if (validCategory().equalsIgnoreCase("true")) {
                    return value;
                } else {
                    return validCategory();
                }
            } else return value;
        } else {
            return value;
        }

    }

    public static void removeObject(int position, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            categoryDetails.remove(position);
        }
        AppSettings appSettings = new AppSettings(context);
        registerCategoryAdapter = new RegisterCategoryAdapter(context, categoryDetails);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        categoryItems.setLayoutManager(linearLayoutManager);
        categoryItems.setAdapter(registerCategoryAdapter);
        try {
            setCategoryValue(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        appSettings.setRegisterArray(registerDetails);
    }

    private void showAddDialog() {
        final Dialog dialog = new Dialog(RegisterActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.add_category_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();


        final Spinner categorySpinner, subCategorySpinner;
        final EditText pricePerhour, quickPitch, experience;
        Button confirmButton;
        categorySpinner = (Spinner) dialog.findViewById(R.id.categorySpinner);
        subCategorySpinner = (Spinner) dialog.findViewById(R.id.subCategorySpinner);
        pricePerhour = (EditText) dialog.findViewById(R.id.pricePerhour);
        quickPitch = (EditText) dialog.findViewById(R.id.quickPitch);
        experience = (EditText) dialog.findViewById(R.id.experience);
        confirmButton = (Button) dialog.findViewById(R.id.confirmButton);
        Utils.setCustomButton(RegisterActivity.this, confirmButton);
        Utils.log(TAG, "categoryDetails: " + fullCategories);
        try {
            ArrayAdapter aa = new ArrayAdapter(this, R.layout.gender_items, categoryName);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            categorySpinner.setAdapter(aa);
        } catch (Exception e) {
            e.printStackTrace();
        }
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int i, long l) {


//                subCategories = fullCategories.optJSONObject(i).optJSONArray("list_subcategory");
//                subcategoryName = new String[subCategories.length()];
//                if (subCategories.length() == 0) {
//                    subcategoryName = new String[1];
//
//                    subcategoryName[0] = "";
//                    ArrayAdapter aa = new ArrayAdapter(RegisterActivity.this, R.layout.gender_items, subcategoryName);
//                    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                    subCategorySpinner.setAdapter(aa);
//                } else {
//                    for (int count = 0; count < subCategories.length(); count++) {
//
//                        subcategoryName[count] = subCategories.optJSONObject(count).optString("sub_category_name");
//                        ArrayAdapter aa = new ArrayAdapter(RegisterActivity.this, android.R.layout.simple_spinner_item, subcategoryName);
//                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                        subCategorySpinner.setAdapter(aa);
//
//                    }
//                }

                JSONObject input = new JSONObject();
                try {
                    input.put("id", fullCategories.optJSONObject(i).optString("id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ApiCall.PostMethodHeaders(RegisterActivity.this, UrlHelper.LIST_SUB_CATEGORY, input, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        JSONObject category = new JSONObject();
                        subCategories = response.optJSONArray("list_subcategory");
                        subcategoryName = new String[subCategories.length()];
                        List<String> list = new ArrayList<String>();
                        for (int in = 0; in < subCategories.length(); in++) {

                            subcategoryName[in] = subCategories.optJSONObject(in).optString("sub_category_name");

                        }
                        ArrayAdapter aa = new ArrayAdapter(RegisterActivity.this, android.R.layout.simple_spinner_item, subcategoryName);
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        subCategorySpinner.setAdapter(aa);

                    }
                });


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (quickPitch.getText().length() > 0) {
                    if (pricePerhour.getText().length() > 0) {
                        if (experience.getText().length() > 0) {
                            if (categoryName[categorySpinner.getSelectedItemPosition()].length() > 0) {
                                if (subcategoryName.length != 0) {
                                    if (subcategoryName[subCategorySpinner.getSelectedItemPosition()].length() > 0) {
                                        JSONObject jsonObject = new JSONObject();
                                        JSONObject jsonObject1 = new JSONObject();
                                        try {
                                            jsonObject.put("categoryMainName", categoryName[categorySpinner.getSelectedItemPosition()]);
                                            jsonObject.put("sub_category_name", subcategoryName[subCategorySpinner.getSelectedItemPosition()]);
                                            jsonObject.put("sub_category_id", subCategories.optJSONObject(subCategorySpinner.getSelectedItemPosition()).optString("id"));
                                            jsonObject.put("quickpitch", quickPitch.getText().toString());
                                            jsonObject.put("priceperhour", pricePerhour.getText().toString());
                                            jsonObject.put("experience", experience.getText().toString());
                                            jsonObject.put("category_id", fullCategories.optJSONObject(categorySpinner.getSelectedItemPosition()).optString("id"));
                                            jsonObject.put("status", "1");
                                            categoryDetails.put(jsonObject);


                                            registerCategoryAdapter = new RegisterCategoryAdapter(RegisterActivity.this, categoryDetails);
                                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RegisterActivity.this, LinearLayoutManager.HORIZONTAL, false);
                                            categoryItems.setLayoutManager(linearLayoutManager);
                                            categoryItems.setAdapter(registerCategoryAdapter);
                                            setCategoryValue(RegisterActivity.this);
                                            appSettings.setRegisterArray(registerDetails);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        dialog.dismiss();
                                    } else {
                                        Utils.toast(RegisterActivity.this, getResources().getString(R.string.please_select_subcategroy));

                                    }
                                } else {
                                    Utils.toast(RegisterActivity.this, getResources().getString(R.string.no_sub_categroy));

                                }
                            } else {
                                Utils.toast(RegisterActivity.this, getResources().getString(R.string.please_select_categroy));

                            }
                        } else {
                            Utils.toast(RegisterActivity.this, getResources().getString(R.string.please_enter_experience));
                        }
                    } else {
                        Utils.toast(RegisterActivity.this, getResources().getString(R.string.please_enter_price_per_ho));
                    }
                } else {
                    Utils.toast(RegisterActivity.this, getResources().getString(R.string.please_enter_quick_pitch));
                }
            }
        });
    }

    public void setDefaultTimeslots(TextView View) {
        View.setText(getResources().getString(R.string.add_time));
        View.setTextSize(14);
        View.setTextColor(Utils.getPrimaryColor(RegisterActivity.this));
    }

    public void setSelectedTimeslots(TextView View, String text) {
        View.setText(text);
        View.setTextSize(11);
        View.setTextColor(getResources().getColor(R.color.grey));
    }

    private void showTimeSlotDialog() {
        final Dialog dialog = new Dialog(RegisterActivity.this);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.time_slot_dialog);
        final RecyclerView timeSlot = (RecyclerView) dialog.findViewById(R.id.timeSlot);
        TextView okayText = (TextView) dialog.findViewById(R.id.okayText);
        okayText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String text = getSelectedTimeslots(jsonArray);
                if (selectedTime == 0) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(mondayView);
                    } else {
                        setSelectedTimeslots(mondayView, text);


                    }
                } else if (selectedTime == 1) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(tuesdayView);
                    } else {
                        setSelectedTimeslots(tuesdayView, text);


                    }
                } else if (selectedTime == 2) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(wednesdayView);

                    } else {

                        setSelectedTimeslots(wednesdayView, text);


                    }
                } else if (selectedTime == 3) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(thursdayView);
                    } else {
                        setSelectedTimeslots(thursdayView, text);


                    }
                } else if (selectedTime == 4) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(fridayView);
                    } else {
                        setSelectedTimeslots(fridayView, text);


                    }
                } else if (selectedTime == 5) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(saturdayView);
                    } else {
                        setSelectedTimeslots(saturdayView, text);


                    }
                } else if (selectedTime == 6) {
                    if (text.length() == 0) {
                        setDefaultTimeslots(sundayView);
                    } else {
                        setSelectedTimeslots(sundayView, text);


                    }
                }

                appSettings.setAvailableSlots(finalArray);
                dialog.dismiss();
            }
        });

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject = jsonArray.optJSONObject(i);
            try {
                jsonObject.put("selected", "false");
                jsonArray.put(i, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        appSettings.setPageNumber("6");

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();


        timeSlotAdapter = new TimeSlotAdapter(RegisterActivity.this, jsonArray, selectedTime);
        GridLayoutManager linearLayoutManager = new GridLayoutManager(RegisterActivity.this, 4, GridLayoutManager.VERTICAL, false);
        timeSlot.setLayoutManager(linearLayoutManager);
        timeSlot.setAdapter(timeSlotAdapter);


    }

    private String getSelectedTimeslots(JSONArray timeSlotDetails) {
        String retuntext = "";
        for (int i = 0; i < timeSlotDetails.length(); i++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject = timeSlotDetails.optJSONObject(i);
            if (jsonObject.optString("selected").equalsIgnoreCase("true")) {
                if (retuntext.length() == 0) {
                    retuntext = jsonObject.optString("timing");

                } else {
                    retuntext = retuntext + "," + jsonObject.optString("timing");

                }
            }

        }

        return retuntext;


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private class GenericTextWatcher implements TextWatcher {
        String editText;

        private GenericTextWatcher(String view) {
            this.editText = view;

        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();

            try {
                if (!isFirstTime) {
                    if (editText.equalsIgnoreCase("1")) {
                        try {
                            setGeneralValues();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (editText.equalsIgnoreCase("2")) {
                        setAddressValues();
                    } else if (editText.equalsIgnoreCase("3")) {
                        setRegisterValue();
                    } else if (editText.equalsIgnoreCase("4")) {
                        setProfileValue();
                    }
                    appSettings.setRegisterArray(registerDetails);
                }

            } catch (Exception e) {

            }
        }
    }
}
