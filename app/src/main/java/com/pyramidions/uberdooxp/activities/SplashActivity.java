package com.pyramidions.uberdooxp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;


import com.pyramidions.uberdooxp.R;
import com.pyramidions.uberdooxp.Volley.ApiCall;
import com.pyramidions.uberdooxp.Volley.VolleyCallback;
import com.pyramidions.uberdooxp.helpers.AppSettings;
import com.pyramidions.uberdooxp.helpers.UrlHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity extends AppCompatActivity {
    AppSettings appSettings = new AppSettings(SplashActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


////        ImageView spinningEarth = (ImageView)findViewById(R.id.bottomWorld);
//        RotateAnimation rotateAnimation = new RotateAnimation(360,0, Animation.RELATIVE_TO_SELF,.5f,Animation.RELATIVE_TO_SELF,.5f);
//        rotateAnimation .setDuration(50000);
//        rotateAnimation .setFillAfter(true);
//        rotateAnimation .setRepeatCount(-1);
////        CycleInterpolator interpolator= new CycleInterpolator();
////        rotateAnimation.setInterpolator(new CycleInterpolator());
//
////        spinningEarth.setAnimation(rotateAnimation);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (appSettings.getIsLogged().equalsIgnoreCase("true")) {
                    upDateToken();
//                    moveMainActivity();
                } else {


                    moveOnBoardActivity();

                }
            }
        }, 3000);
    }

    private void upDateToken() {
        ApiCall.PostMethodHeaders(SplashActivity.this, UrlHelper.UPDATE_DEVICE_TOKEN, getInputs(), new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                moveMainActivity();
            }
        });
    }


    private JSONObject getInputs() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fcm_token", appSettings.getFireBaseToken());
            jsonObject.put("os", "android");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    private void moveOnBoardActivity() {
        Intent onboard = new Intent(SplashActivity.this, SignInActivity.class);
        startActivity(onboard);
        finish();
    }

    private void moveMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.putExtra("type", "new");
        startActivity(intent);
        finish();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
