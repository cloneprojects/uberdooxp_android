package com.pyramidions.uberdooxp.activities;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.pyramidions.uberdooxp.R;
import com.pyramidions.uberdooxp.Volley.ApiCall;
import com.pyramidions.uberdooxp.Volley.VolleyCallback;
import com.pyramidions.uberdooxp.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONObject;


public class AboutUs extends AppCompatActivity {
    WebView webView;
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        webView = (WebView) findViewById(R.id.webView);
        final TextView textView = (TextView) findViewById(R.id.textView);
        backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ApiCall.PostMethodWithNoParams(AboutUs.this, UrlHelper.ABOUT_US, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                if (response.has("page")) {
                    JSONArray jsonArray = response.optJSONArray("page");
                    Log.e("null", "onSuccess: " + jsonArray);
                    textView.setText(jsonArray.optJSONObject(0).optString("termsAndCondition"));
                }
//                JSONArray static_pages = response.optJSONArray("static_pages");
//                JSONObject jsonObject = new JSONObject();
//
//                String url = jsonObject.optString("page_url");
//                webView.getSettings().setJavaScriptEnabled(false);
//                webView.loadUrl(url);
//                webView.setHorizontalScrollBarEnabled(false);
            }
        });


    }
}
