package com.pyramidions.uberdooxp.helpers;

import android.content.Context;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;

/*
 * Created by user on 26-10-2017.
 */

public class AppSettings {
    private Context context;
    private String isLogged;
    private String token;
    private String fireBaseToken;
    private String imageUploadPath;
    private String pageNumber;

    private String providerId;
    private JSONArray registerArray = new JSONArray();
    private JSONArray timeSlots = new JSONArray();
    private JSONArray availableSlots = new JSONArray();

    public String getImageUploadPath() {
        imageUploadPath = SharedHelper.getKey(context, "imageUploadPath");

        return imageUploadPath;
    }

    public void setImageUploadPath(String imageUploadPath) {
        SharedHelper.putKey(context, "imageUploadPath", imageUploadPath);

        this.imageUploadPath = imageUploadPath;
    }

    public String getProviderId() {
        providerId = SharedHelper.getKey(context, "providerId");

        return providerId;
    }

    public void setProviderId(String providerId) {
        SharedHelper.putKey(context, "providerId", providerId);

        this.providerId = providerId;
    }


    public JSONArray getRegisterArray() {
        try {
            registerArray = new JSONArray(SharedHelper.getKey(context, "registerArray"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return registerArray;
    }

    public void setRegisterArray(JSONArray registerArray) {
        SharedHelper.putKey(context, "registerArray", registerArray.toString());

        this.registerArray = registerArray;
    }


    public JSONArray getAvailableSlots() throws JSONException {
        availableSlots = new JSONArray(SharedHelper.getKey(context, "registerArray"));

        return availableSlots;
    }

    public void setAvailableSlots(JSONArray availableSlots) {
        SharedHelper.putKey(context, "availableSlots", availableSlots.toString());

        this.availableSlots = availableSlots;
    }

    public JSONArray getTimeSlots() {
        try {
            timeSlots = new JSONArray(SharedHelper.getKey(context, "timeSlots"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return timeSlots;
    }

    public void setTimeSlots(JSONArray timeSlots) {
        SharedHelper.putKey(context, "timeSlots", timeSlots.toString());
        this.timeSlots = timeSlots;
    }

    public String getPageNumber() {
        pageNumber = SharedHelper.getKey(context, "pageNumber");

        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        SharedHelper.putKey(context, "pageNumber", pageNumber);

        this.pageNumber = pageNumber;
    }

    public String getIsLogged() {
        isLogged = SharedHelper.getKey(context, "isLogged");
        return isLogged;
    }

    public void setIsLogged(String isLogged) {
        SharedHelper.putKey(context, "isLogged", isLogged);
        this.isLogged = isLogged;
    }

    public AppSettings(Context context) {

        this.context = context;
    }

    public String getToken() {
        token = SharedHelper.getKey(context, "token");
        return token;
    }

    public void setToken(String token) {
        SharedHelper.putKey(context, "token", token);
        this.token = token;
    }

    public String getFireBaseToken() {
        fireBaseToken = SharedHelper.getKey(context, "fireBaseToken");
        return fireBaseToken;
    }

    public void setFireBaseToken(String fireBaseToken) {
        SharedHelper.putKey(context, "fireBaseToken", fireBaseToken);
        this.fireBaseToken = fireBaseToken;
    }

}
