package com.pyramidions.uberdooxp.helpers;

/**
 * Created by user on 22-09-2017.
 */

import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Handler;


import com.pyramidions.uberdooxp.application.UberdooXP;

import java.util.ArrayList;

/**
 * Created by Jack on 2015/10/19.
 */

/**
 * Created by Jack on 2015/10/17.
 */

public class BallClipRotateMultipleIndicator extends Indicator {

    float scaleFloat = 2, degrees;

    Handler blink = new Handler();
    private boolean notBlink = false;
    private long lastUpdateTime;
    private long blinkStart;

    @Override
    public void draw(final Canvas canvas, Paint paint) {
        paint.setStrokeWidth(25);
        paint.setStyle(Paint.Style.STROKE);

        float circleSpacing = 12;
        float x = getWidth() / 2;
        float y = getHeight() / 2;

        canvas.save();

        canvas.translate(x, y);
        canvas.scale(scaleFloat, scaleFloat);
        canvas.rotate(degrees);

        //draw two big arc
        float[] bStartAngles = new float[]{135, 45};
        for (int i = 0; i < 2; i++) {
            RectF rectF = new RectF(-x + circleSpacing + 20, -y + circleSpacing - 20, x - circleSpacing - 20, y - circleSpacing - 20);
            canvas.drawArc(rectF, bStartAngles[i], 90, false, paint);
        }


        paint.setStrokeWidth(15);
        paint.setStyle(Paint.Style.STROKE);
        canvas.restore();
        canvas.translate(x, y);
        canvas.scale(scaleFloat, scaleFloat);
        canvas.rotate(-degrees);
        //draw two small arc


        float[] sStartAngles = new float[]{135, 45};
        float center = 0;
        for (int i = 0; i < 2; i++) {
            RectF rectF = new RectF(-x / 1.8f + circleSpacing, -y / 1.8f + circleSpacing, x / 1.8f - circleSpacing, y / 1.8f - circleSpacing);
            canvas.drawArc(rectF, sStartAngles[i], 90, false, paint);
            float height = rectF.height();
            float width = rectF.width();
            center = (height + width) / 2;
        }

        canvas.restore();
        final Paint paint1 = new Paint();
        final Paint paint2 = new Paint();
        Typeface face = Typeface.createFromAsset(UberdooXP.getContext().getAssets(), "fonts/Ubuntu-Regular.ttf");
        paint1.setColor(Color.WHITE);
        paint1.setTextSize(55);
        paint1.setTypeface(face);
        final float finalCenter = center;
        if (notBlink) {
            canvas.drawText("Loading...", (finalCenter - 130), finalCenter, paint1);
        } else {
            canvas.drawText("", (finalCenter - 130), finalCenter, (paint2));
        }


    }

    @Override
    public ArrayList<ValueAnimator> onCreateAnimators() {
        ArrayList<ValueAnimator> animators = new ArrayList<>();
        ValueAnimator scaleAnim = ValueAnimator.ofFloat(1, 0.6f, 1);
        scaleAnim.setDuration(1000);
        scaleAnim.setRepeatCount(-1);
        addUpdateListener(scaleAnim, new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                scaleFloat = (float) animation.getAnimatedValue();
                postInvalidate();
            }
        });

        ValueAnimator rotateAnim = ValueAnimator.ofFloat(0, 180, 360);
        rotateAnim.setDuration(1000);
        rotateAnim.setRepeatCount(-1);
        addUpdateListener(rotateAnim, new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                degrees = (float) animation.getAnimatedValue();
                postInvalidate();
            }
        });

        if (System.currentTimeMillis() - lastUpdateTime >= 150
                && notBlink) {
            notBlink = false;
            blinkStart = System.currentTimeMillis();
        }
        if (System.currentTimeMillis() - blinkStart >= 150 && !notBlink) {
            notBlink = true;
            lastUpdateTime = System.currentTimeMillis();
        }

        animators.add(scaleAnim);
        animators.add(rotateAnim);
        return animators;
    }

}